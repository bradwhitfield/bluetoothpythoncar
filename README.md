# README #

This README will guide you through installing this service on a Raspberry Pi running Arch Linux. The script is Python 3, so it can be easily ported to other platforms, but it does not currently have any packages for any distribution.

### What is this repository for? ###

* This is the Bluetooth server for the remote control Raspberry Pi.
* 0.9
* Pictures and demo coming soon

### Software Prerequisites ###

* Python3
* Systemd (others are possible)
* PyBluez for Python3

### Hardware Requirements ###

* Raspberry Pi B+ (A+ not tested, but would theoretically work)
* Bluetooth adapter
* motor control board
* motors with tires
* platform to drive on
* LEDs optional.

### How do I get set up? ###

* Download the file onto the Raspberry Pi
* Install the needed software dependencies
* Copy the pythoncar.service file to the "/usr/lib/systemd/system/" directory
* Run "systemctl enable pythoncar.service"
* Use [Bluetoothctl](https://wiki.archlinux.org/index.php/Bluetooth#Configuration_via_the_CLI) to allow the mobile device of choice
* Restart the Pi

You should be good to go at this point, assuming you have [the app](https://bitbucket.org/bradwhitfield/rpiremotecontrolapp) installed already.

### More to Come!!!! ###
* Better code
* AUR package
* Material design
* Demos and pictures of the thing in action
* Probably other stuff