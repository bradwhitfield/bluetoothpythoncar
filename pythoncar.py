__author__ = 'Tyler Kosaski & Brad Whitfield'
import bluetooth
# A program to control the movement of a single motor using the RTK MCB!
# Composed by The Raspberry Pi Guy to accompany his tutorial!

# Let's import the modules we will need!
import time
import RPi.GPIO as GPIO

# Next we setup the pins for use!
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(13,GPIO.OUT)
GPIO.setup(17,GPIO.OUT)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(19,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)
GPIO.setup(23,GPIO.OUT)
GPIO.setup(21,GPIO.OUT) #right break light
GPIO.setup(20,GPIO.OUT) #left brake light
GPIO.setup(26,GPIO.OUT)

UUID = "8dc4b250-2483-4e66-b10c-1b8a60de64e2"
SERVICE_NAME = "BluetoothRPiRemote"

headLightsOn = False
glowLightsOn = False
leftSigOn = False
rightSigOn = False
sleeptime = 0.025

print('Starting motor sequence!')

while True:

    GPIO.output(20, True)
    GPIO.output(21, True)

    serverSocket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    serverSocket.bind(("", bluetooth.PORT_ANY))
    serverSocket.listen(1)

    port = serverSocket.getsockname()[1]

    bluetooth.advertise_service(serverSocket, SERVICE_NAME, UUID,
                                service_classes=[UUID, bluetooth.SERIAL_PORT_CLASS],
                                profiles=[bluetooth.SERIAL_PORT_PROFILE],
                                )

    #Attempt Bluetooth Connection
    print("Waiting for user to connect.")
    clientSocket, clientInfo = serverSocket.accept()
    print("WE GOTS A CONNECTION! ", clientInfo)

    while True:
        print("Waiting for stuff from Bluetooth.")

        try:
            data = clientSocket.recv(1024)
            print("Received data: [%s]" % data)
            
            if data == b'8':

                #turn off brakelights

                GPIO.output(20, False)
                GPIO.output(21, False)

                # Makes the motor spin forward

                GPIO.output(17, True)
                GPIO.output(18, False)
                GPIO.output(22, True)
                GPIO.output(23, False)
                time.sleep(sleeptime)

            elif data == b'2':

                #turn off brakelights

                GPIO.output(20, False)
                GPIO.output(21, False)

                # Spins backwards for 3 seconds
                GPIO.output(17, False)
                GPIO.output(18, True)
                GPIO.output(22, False)
                GPIO.output(23, True)
                # reverse lights
                GPIO.output(26, True)


                time.sleep(sleeptime)
                GPIO.output(26, False)


            elif data == b'4':

                #turn off brakelights

                GPIO.output(20, False)
                GPIO.output(21, False)

                #turns left
                GPIO.output(17, False)
                GPIO.output(18, False)
                GPIO.output(22, True)
                GPIO.output(23, False)
                time.sleep(sleeptime)


            elif data == b'6':

                #turn off brakelights

                GPIO.output(20, False)
                GPIO.output(21, False)

                #turns right
                GPIO.output(17, True)
                GPIO.output(18, False)
                GPIO.output(22, False)
                GPIO.output(23, False)
                time.sleep(sleeptime)

            elif data == b'1': #headlights
                if(headLightsOn):
                    GPIO.output(13, False)
                    headLightsOn = False
                else:
                    GPIO.output(13, True)
                    headLightsOn = True

            elif data == b'0': #glowlights
                if(glowLightsOn):
                    GPIO.output(19, False)
                    glowLightsOn = False
                else:
                    GPIO.output(19, True)
                    glowLightsOn = True


            elif data == b'7': #left blinker
                if(leftSigOn):
                    GPIO.output(20, False)
                    leftSigOn = False
                else:
                    leftSigOn = True
                    while leftSigOn:
                        GPIO.output(20, True)
                        time.sleep(.5)
                        GPIO.output(20, False)
                        time.sleep(.5)
                        GPIO.output(20, True)
                        #TODO: Make thread
                        leftSigOn = False


            elif data == b'9': #right blinker
                if(rightSigOn):
                    GPIO.output(21, False)
                    rightSigOn = False
                else:
                    rightSigOn = True
                    while rightSigOn:
                        GPIO.output(21, True)
                        time.sleep(.5)
                        GPIO.output(21, False)
                        time.sleep(.5)
                        GPIO.output(21, True)
                        #TODO: Make thread
                        rightSigOn = False
            elif data == b'5': #stop

              #brakelights on
              GPIO.output(20, True)
              GPIO.output(21, True)


              GPIO.output(17, False)
              GPIO.output(18, False)
              GPIO.output(22, False)
              GPIO.output(23, False)
              
            elif data == b'-1': #restart
              GPIO.output(13, False)
              GPIO.output(19, False)
              GPIO.output(17, False)
              GPIO.output(18, False)
              GPIO.output(20, False)
              GPIO.output(21, False)
              GPIO.output(22, False)
              GPIO.output(23, False)
              GPIO.output(26, False)
              break

            else:
                print("That was not a valid entry")

        except IOError:
            print("IOERROR")
            #brakelights on
            GPIO.output(13, False)
            GPIO.output(19, False)
            GPIO.output(17, False)
            GPIO.output(18, False)
            GPIO.output(20, False)
            GPIO.output(21, False)
            GPIO.output(22, False)
            GPIO.output(23, False)
            serverSocket.close()
            clientSocket.close()
            break


        except KeyboardInterrupt:
            print("Closing out!")

            serverSocket.close()
            clientSocket.close()

            # If a keyboard interrupt is detected then it exits cleanly!
            print('Finishing up!')
            # turn off everything
            GPIO.output(13, False)
            GPIO.output(19, False)
            GPIO.output(17, False)
            GPIO.output(18, False)
            GPIO.output(20, False)
            GPIO.output(21, False)
            GPIO.output(22, False)
            GPIO.output(23, False)
            serverSocket.close()
            clientSocket.close()

            break


    serverSocket.close()
    clientSocket.close()
